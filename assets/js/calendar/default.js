document.addEventListener('DOMContentLoaded', function() {


    $('.calendar-page-header').hide()// hide the header in calendar.php
    
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
      customButtons: {
        myCustomButton: {
          text: 'Next',
          click: function() {
            calendar.next();
            var link = $('a.js-modal-large').attr('href');
           var date= $('a.fc-day-number').attr('data-goto');
           $('a.fc-day-number').addClass('js-modal-large');
           $('a.fc-day-number').attr('href', link);
           $('a.fc-day-number').removeAttr('data-goto');
           $('.fc-day-top').click(function(){
            var datetogo=$(this).attr('data-date');
            var currentUrl = link;
            $('a.js-modal-large').attr('href',currentUrl+'&duedate='+datetogo);
          })
          }
        },
        myCustomButton2: {
          text: 'Prev',
          click: function() {
            calendar.prev();
            var link = $('a.js-modal-large').attr('href');
           var date= $('a.fc-day-number').attr('data-goto');
           $('a.fc-day-number').addClass('js-modal-large');
           $('a.fc-day-number').attr('href', link);
           $('a.fc-day-number').removeAttr('data-goto');
           $('.fc-day-top').click(function(){
            var datetogo=$(this).attr('data-date');
            var currentUrl = link;
            $('a.js-modal-large').attr('href',currentUrl+'&duedate='+datetogo);
          })
          }
        },
        myCustomButton3: {
          text: 'Today',
          click: function() {
            calendar.today();
            var link = $('a.js-modal-large').attr('href');
           var date= $('a.fc-day-number').attr('data-goto');
           $('a.fc-day-number').addClass('js-modal-large');
           $('a.fc-day-number').attr('href', link);
           $('a.fc-day-number').removeAttr('data-goto');
           $('.fc-day-top').click(function(){
            var datetogo=$(this).attr('data-date');
            var currentUrl = link;
            $('a.js-modal-large').attr('href',currentUrl+'&duedate='+datetogo);
          })
          }
        }
      },
      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' , 'bootstrap'],
    //   defaultDate: '2019-04-12',
      header: {
        left: 'myCustomButton2,myCustomButton,myCustomButton3',
        center: '',
        right: 'title'
      },
      defaultView: 'dayGridMonth',
      selectable: true,
      // editable: true,
      navLinks: true,
      eventLimit: true, // allow "more" link when too many events
      events: {
        url: 'http://localhost/kanboard/app/Controller/event.php',
        extraParams: {user_id: getUrlVars()["user_id"]},
        type: 'POST', // Send post data
        error: function() {
            alert('There was an error while fetching events.');
        }
      },
      eventDrop: function(info) {
        alert(info.event.title + " was dropped on " + info.event.start.toISOString());
    
        if (!confirm("Are you sure about this change?")) {
          info.revert();
        }
      },
      loading: function(bool) {
        document.getElementById('loading').style.display =
          bool ? 'block' : 'none';
      },
      });
      calendar.render();
      var link = $('a.js-modal-large').attr('href');
      var date= $('a.fc-day-number').attr('data-goto');
      $('a.fc-day-number').addClass('js-modal-large');
      $('a.fc-day-number').attr('href', link);
      $('a.fc-day-number').removeAttr('data-goto');


      $('.fc-day-top').click(function(){
        var datetogo=$(this).attr('data-date');
        var currentUrl = link;
        $('a.js-modal-large').attr('href',currentUrl+'&duedate='+datetogo);
      })

    });
 
    function getUrlVars()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

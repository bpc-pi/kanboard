<?php

namespace Kanboard\Controller;
use Kanboard\Core\Controller\PageNotFoundException;

/**
 * Dashboard Controller
 *
 * @package  Kanboard\Controller
 * @author   Frederic Guillot
 */
class CalendarController extends BaseController
{
    public function index(){
        
        $user = $this->getUser();
       
        $this->response->html($this->helper->layout->dashboard('dashboard/calendar',array(
            'title'              => t('Dashboard for %s', $this->helper->user->getFullname($user)),
            'user'               => $user,
            'overview_paginator' => $this->dashboardPagination->getOverview($user['id']),
            'project_paginator'  => $this->projectPagination->getDashboardPaginator($user['id'], 'show', 10),
        )));
    }
    //fetch the projects of the particular user and show in the select box.
    public function select(){
        $projects=$this->fetch();
        $this->response->html($this->helper->layout->calendar('calendar/projectselect', array('projects'=>$projects)));
    }
    public function fetch(){
        $user = $this->getUser();
        $projects=$this->projectUserRoleModel->projects($user);
        return $projects;  
    }
    //task create through calendar date link.
    public function createtask()
    {
        $data=$_POST['projects'];
        $project=$this->projectModel->projectname($data);
        $errors=array('name'=>'koel');
        $swimlanesList = $this->swimlaneModel->getList($project[0]['id'], false, true);

        $values = $this->prepareValues($project[0]['is_private'], $swimlanesList);
       
        $values = $this->hook->merge('controller:task:form:default', $values, array('default_values' => $values));
        $values = $this->hook->merge('controller:task-creation:form:default', $values, array('default_values' => $values));
        
        $this->response->html($this->helper->layout->calendar1('calendar/taskcreate', array(
            'project' => $project,
            'errors' => $errors,
            'values' => $values + array('project_id' => $project[0]['id']),
            'columns_list' => $this->columnModel->getList($project[0]['id']),
            'users_list' => $this->projectUserRoleModel->getAssignableUsersList($project[0]['id'], true, false, $project[0]['is_private'] == 1),
            'categories_list' => $this->categoryModel->getList($project[0]['id']),
            'swimlanes_list' => $swimlanesList,
        ))); 
        
        // $this->response->html($this->template->render('calendar/taskcreate', array('project'=>$project)));
    }

    protected function prepareValues($isPrivateProject, array $swimlanesList)
    {
        $values = array(
            'swimlane_id' => $this->request->getIntegerParam('swimlane_id', key($swimlanesList)),
            'column_id'   => $this->request->getIntegerParam('column_id'),
            'color_id'    => $this->colorModel->getDefaultColor(),
        );

        if ($isPrivateProject) {
            $values['owner_id'] = $this->userSession->getId();
        }

        return $values;
    }
}

<?php
$id=$_GET['user_id'];

try { 
// Connect to database
$db_host = "localhost";
$db_name = "kanboard";
$db_user = "kanboard";
$db_pass = "kanboard007";

$connection = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);

 $query = "SELECT * FROM tasks where column_id in (select id from columns where position !=4 and position !=5) and owner_id=$id OR owner_id= 0 and creator_id=$id";
 $sth = $connection->prepare($query);
 $sth->execute();

 $query2= "SELECT `value` FROM `settings` where `option` = 'application_timezone'";
 $sql=$connection->prepare($query2);
 $sql->execute();
 $timezone=$sql->fetch(PDO::FETCH_ASSOC)['value'];
 
$events=array();
date_default_timezone_set($timezone);
while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {

    $e = array();
    $e['id'] = $row['id'];
    $e['title'] = $row['title'];
    $e['start'] = date('Y-m-d',$row['date_due']);

    array_push($events, $e);

};

echo json_encode($events);
exit();

} catch(PDOException $e){
    echo $e->getMessage();
}



<?php
$db_host = "localhost";
$db_name = "kanboard";
$db_user = "kanboard";
$db_pass = "kanboard007";
$connection = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);

$today=idate('U',time());
$query1= "SELECT * FROM tasks where `date_due`>$today and `notification` is not null";
$sth = $connection->prepare($query1);
$sth->execute();

$tasks=array();
while($row=$sth->fetch(PDO::FETCH_ASSOC)){
    $e=array();
    $e['id']=$row['id'];
    $e['title']=$row['title'];
    $e['date_due']=$row['date_due'];
    $e['project_id']=$row['project_id'];
    $e['creator_id']=$row['creator_id'];
    $e['owner_id']=$row['owner_id'];
    $e['column_id']=$row['column_id'];
    $e['notification']=$row['notification'];
    array_push($tasks,$e);
}
date_default_timezone_set('Asia/Kolkata');
$i=0;
foreach($tasks as $task)
{
    $today1=new DateTime(date('y-m-d'));
    $duedate=new DateTime(date('y-m-d',$task['date_due']));
    $diffs=$duedate->diff($today1);
    $diff = $diffs->format('%a');
    $tasks[$i]['date_diff']=$diff;
    $i++;
}

$query2="SELECT `value` from `kanboard`.`settings` WHERE `option`='telegram_apikey' or `option`='telegram_username'";
$sql = $connection->prepare($query2);
$sql->execute();
$data=$sql->fetchAll(PDO::FETCH_ASSOC);

$apikey=$data[0]['value'];
$bot_username=$data[1]['value'];

foreach ($tasks as $task)
{
    $db_host = "localhost";
    $db_name = "kanboard";
    $db_user = "kanboard";
    $db_pass = "kanboard007";
    $connection = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
    
    $id=$task['id'];
    $title=$task['title'];
    $duedate=date('d/m/y',$task['date_due']);
    $project_id=$task['project_id'];
    $creator_id=$task['creator_id'];
    $owner_id=$task['owner_id'];
    $column_id=$task['column_id'];
    
    $query3="SELECT `value` FROM project_has_metadata where `project_id`=$project_id";
    $sql1 = $connection->prepare($query3);
    $sql1->execute();
    $chat_id=$sql1->fetch(PDO::FETCH_ASSOC)['value'];
    
    $query4="SELECT `name` from users where id=$creator_id";
    $sql2 = $connection->prepare($query4);
    $sql2->execute();
    
    $query5="SELECT `name` from users where id=$owner_id";
    $sql3 = $connection->prepare($query5);
    $sql3->execute();
    $owner_name=$sql3->fetch(PDO::FETCH_ASSOC)['name'];

    $query6="SELECT `position` from columns where id=$column_id";
    $sql4 = $connection->prepare($query6);
    $sql4->execute();
    $position=$sql4->fetch(PDO::FETCH_ASSOC)['position'];

    if($task['notification']==$task['date_diff'] && $position!=4 && $position!=5){
        send_Message($apikey, $bot_username, $chat_id, $id, $project_id, $title, $duedate, $owner_name, $creator_name);
    }
}        

function send_Message($apikey, $bot_username, $chat_id, $id, $project_id, $title, $duedate, $owner_name, $creator_name)
{   
    $taskurl="http://localhost/kanboard/?controller=TaskViewController&action=show&task_id=".$id."&project_id=".$project_id;
    $message="Hi this is Koel!";
        
    require_once "../../vendor/autoload.php";
    try
    {   
        // Create Telegram API object
        $telegram = new \TelegramBot\Api\BotApi($apikey);

        $telegram->sendMessage($chat_id, $message);

        // Message pay load
        //$data = array('chat_id' => $chat_id, 'text' => $message, 'parse_mode' => 'HTML');
        
        // Send message
        //$result = Request::sendMessage($data);
    } 
    catch (\TelegramBot\Api\Exception $e) 
    {
        // log telegram errors
        error_log($e->getMessage());
    }

}

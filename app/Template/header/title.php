<h1>
    <span class="logo">
        <?= $this->url->link('<img src="http://localhost/kanboard/Images/logo.png" height=45 />', 'DashboardController', 'show', array(), false, '', t('Dashboard')) ?>
    </span>   
</h1>

<div class="title_top">
    <h4 class="title">
        <?php if (! empty($project) && ! empty($task)): ?>
            <?= $this->url->link($this->text->e($project['name']), 'BoardViewController', 'show', array('project_id' => $project['id'])) ?>
        <?php else: ?>
            <?= $this->text->e($title) ?>
        <?php endif ?>
        <?php if (! empty($description)): ?>
        <small class="tooltip" title="<?= $this->text->markdownAttribute($description) ?>">
            <i class="fa fa-info-circle"></i>
        </small>
    <?php endif ?>
    </h4>
</div>app.min

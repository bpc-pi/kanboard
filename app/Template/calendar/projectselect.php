<section id="main">
    <div class="page-header">
        <div class="">
            <h2>Select your project</h2>
            <form action="<?= $this->url->href('CalendarController', 'createtask') ?>">
            <?= $this->form->csrf() ?>
            <select name="projects" id="selectproject">
            <?php foreach ($projects as $project):?>
                <option value="<?= $project['id'] ?>"><?= $project['name'] ?></option>
            <?php endforeach?>
            </select>
            <input type="hidden" name="duedate" value="<?= $_GET['duedate'] ?>">
            <?= $this->modal->submitButtons() ?>
            </form>
        </div>
    </div>
</section>

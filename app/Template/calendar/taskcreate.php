<div class="page-header">
    <h2><?= $project['project'][0]['name']?> &gt; <?= t('New task') ?></h2>
</div>

<form method="post" action="<?= $this->url->href('TaskCreationController', 'save', array('project_id' => $project['project'][0]['id'])) ?>" autocomplete="off">
    <?= $this->form->csrf() ?>

<div class="task-form-container">
        <div class="task-form-main-column">
            <?= $this->task->renderTitleField($project['values'],$project['errors']) ?>
            <?= $this->task->renderDescriptionField($project['values'],$project['errors']) ?>
            <?= $this->task->renderDescriptionTemplateDropdown($project['project'][0]['id']) ?>
            <?= $this->task->renderTagField1($project['project']) ?>
            
            <?= $this->hook->render('template:task:form:first-column', array('values' => $project['values'], 'errors' => $project['errors'])) ?>
        </div>

        <div class="task-form-secondary-column">
            <?= $this->task->renderColorField($project['values']) ?>
            <?= $this->task->renderAssigneeField($project['users_list'], $project['values']) ?>
            <?= $this->task->renderCategoryField($project['categories_list'], $project['values']) ?>
            <?= $this->task->renderSwimlaneField($project['swimlanes_list'],$project['values']) ?>
            <?= $this->task->renderColumnField($project['columns_list'], $project['values']) ?>
            <?= $this->task->renderPriorityField1($project['project'], $project['values']) ?>

            <?= $this->hook->render('template:task:form:second-column', array('values' => $project['values'], 'errors' => $project['errors'])) ?>
        </div>

        <div class="task-form-secondary-column">
            <label for="date_due">Due Date</label><input type="date" name="date_due" value="<?=$_POST['duedate']?>">
            <?= $this->task->renderNotificationField($project['values'], $project['errors'])?>
            <?= $this->task->renderStartDateField($project['values'], $project['errors']) ?>
            <?= $this->task->renderTimeEstimatedField($project['values'], $project['errors']) ?>
            <?= $this->task->renderTimeSpentField($project['values'], $project['errors']) ?>
            <?= $this->task->renderScoreField($project['values'], $project['errors']) ?>
            <?= $this->task->renderReferenceField($project['values'], $project['errors']) ?>

            <?= $this->hook->render('template:task:form:third-column', array('values' => $project['values'], 'errors' => $project['errors'])) ?>
        </div>

        <div class="task-form-bottom">
        <?php if (! isset($duplicate)): ?>
                <?= $this->form->checkbox('another_task', t('Create another task'), 1, isset($values['another_task']) && $values['another_task'] == 1) ?>
                <?= $this->form->checkbox('duplicate_multiple_projects', t('Duplicate to multiple projects'), 1) ?>
        <?php endif ?>
        <?= $this->modal->submitButtons() ?>
        </div>
</div>
</form>
